package com.itau.comercio.controllers;

import java.util.Optional;
import java.util.UUID;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.comercio.models.Usuario;
import com.itau.comercio.repositories.UsuarioRepository;
import com.itau.comercio.services.JWTService;
import com.itau.comercio.services.PasswordService;

@RestController
public class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@Autowired
	JWTService jwtService;
	
	@RequestMapping(method=RequestMethod.POST, path="/usuario")
	public ResponseEntity<?> criarUsuario(@RequestBody Usuario usuario) {
		
		Usuario user = new Usuario();
		String hash = passwordService.gerarHash(usuario.getSenha());
		
		user.setId(UUID.randomUUID());
		user.setNome(usuario.getNome());
		user.setUsername(usuario.getUsername());
		user.setEmail(usuario.getEmail());
		user.setSenha(hash);
		
		usuarioRepository.save(user);
		
		return ResponseEntity.status(201).body(user);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/login")
	public ResponseEntity<?> fazerLogin(@RequestBody Usuario usuario){
		
		Optional<Usuario> usuarioComercioOptional = usuarioRepository.findByUsername(usuario.getUsername());
		Usuario usuarioComercio = usuarioComercioOptional.get(); 
		
		if (!usuarioComercioOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		if(passwordService.verificarHash(usuario.getSenha(), usuarioComercio.getSenha())) {
			String token = jwtService.gerarToken(usuarioComercio.getUsername());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");

			return new ResponseEntity<Usuario>(usuarioComercio, headers, HttpStatus.OK);
		}
		return ResponseEntity.badRequest().build();
	}

	@RequestMapping(path="/verificar")
	public ResponseEntity<?> verificarToken(HttpServletRequest request){
		String token = request.getHeader("Authorization");
		
		token = token.replace("Bearer ", "");
		
		String username = jwtService.validarToken(token);
		
		if(username == null) {
			return ResponseEntity.status(403).build();
		}
		
		Optional<Usuario> usuarioOptional = usuarioRepository.findByUsername(username);
		
		if(!usuarioOptional.isPresent()) {
			return ResponseEntity.status(403).build();
		}
		
		return ResponseEntity.ok().build();
	}

	
}
