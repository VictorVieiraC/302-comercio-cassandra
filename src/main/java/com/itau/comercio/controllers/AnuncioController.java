package com.itau.comercio.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.comercio.models.Anuncio;
import com.itau.comercio.repositories.AnuncioRepository;

@RestController
@RequestMapping("/usuario")
public class AnuncioController {
	
	@Autowired
	AnuncioRepository anuncioRepository;
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<?> criarAnuncio(@RequestBody Anuncio anuncio){
		
		Anuncio anuncioFeito = new Anuncio();
		anuncioFeito.setDescricao(anuncio.getDescricao());
		anuncioFeito.setTitulo(anuncio.getTitulo());
		anuncioFeito.setPreco(anuncio.getPreco());
		anuncioFeito.setUsuario(anuncio.getUsuario());
		
		anuncioRepository.save(anuncioFeito);
		
		return ResponseEntity.status(201).body(anuncioFeito);
	}

}
