package com.itau.comercio.models;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Tables;
import org.springframework.context.annotation.Primary;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Usuario {

	@PrimaryKey
	private UUID id;
	
	@NotNull
	private String nome;
	
	@NotNull
	private String username;
	
	@NotNull
	private String email;
	
	@NotNull
	private String senha;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
