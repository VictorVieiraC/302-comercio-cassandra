package com.itau.comercio.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.comercio.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, UUID>{
	public Optional<Usuario> findByUsername(String username);
}
