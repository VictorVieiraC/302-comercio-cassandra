package com.itau.comercio.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.comercio.models.Anuncio;

public interface AnuncioRepository extends CrudRepository<Anuncio, UUID>{

}
